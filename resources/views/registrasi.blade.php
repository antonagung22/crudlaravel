@extends('layout.main')

@section('title', 'Tabel Mahasiswa')
@section('content')
    <section>
            <div class="conten">
                <h2>Registrasi</h2>
                
                <div class="newdata">
                    <h3>New Data</h3>
                    <form action="/simpan" method="POST">
                        {{csrf_field()}}
                        <table class="tablenewdata">
                            <tr>
                                <td>NAME</td>
                                <td> <input type="text" name="nama_mahasiswa" required="required" placeholder="Nama Lengkap" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>NIM</td>
                                <td> <input type="text" name="nim_mahasiswa"  required="required"placeholder="NIM" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>KELAS</td>
                                <td> <input type="text" name="kelas_mahasiswa" required="required" placeholder="Kelas" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>PRODI</td>
                                <td> <input type="text" name="prodi_mahasiswa"  required="required"placeholder="Prodi" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>FAKULTAS</td>
                                <td> <input type="text" name="fakultas_mahasiswa" required="required" placeholder="Falkultas" class="form-control">
                                </td>
                            </tr>
                        </table>
                        <input type="submit" value="DAFTAR" class="save">
                    </form>
                </div>

                <div class="studentsdata">
                    <table class="tablestudentsdata">
                        <tr class="segmen">
                            <td>NAMA</td>
                            <td>NIM</td>
                            <td>KELAS</td>
                            <td>PRODI</td>
                            <td>FAKULTAS</td>
                            <td>OPTION</td>
                        </tr>
                        @foreach ($data_mhs as $data)
                            <tr>
                                <td>{{$data->nama_mahasiswa}}</td>
                                <td>{{$data->nim_mahasiswa}}</td>
                                <td>{{$data->kelas_mahasiswa}}</td>
                                <td>{{$data->prodi_mahasiswa}}</td>
                                <td>{{$data->fakultas_mahasiswa}}</td>
                                <td>
                                    <a href="/edit/{{$data->id}}" class="btn btn-warning text-white">EDIT</a>
                                    <a href="/hapus/{{$data->id}}"  class="btn btn-danger text-white" >HAPUS</a>                              
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>

            </div>
    </section>
@endsection
