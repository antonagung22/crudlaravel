<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href="{{ asset ('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <script>
        var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
        var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
        return new bootstrap.Popover(popoverTriggerEl)
    })
</script>
</head>
<body>
    <header class="main-header">
        <nav class="navbar navbar-dark bg-dark">
            <span class="navbar-brand mb-0 h1" style="margin-left: 350px;">Tabel Mahasiswa</span>
         </nav>
    </header>

    <section class="frontDesk">
    <div class="container">
        <section class="content-header">
            <ol class="breadcrumb">
            <li>
                <i class="fa fa-calendar"></i>
                <?php
                    $tgl=date('l, d-m-Y');
                    echo $tgl;
                ?>
                </li>
            </ol>
        </section>
        
    <div class="container">
    {{-- header --}}
     @yield('content')
     
     
     <!-- footer -->
      
         <div class="card mt-3">
            <div class="card-body center">
            <div class="container">
                <center><b>Copyright</b> © <b>2021</b><b> Antonie So-Hyun</b></center>
            </div>

         <!-- Akhir Card Tabel --->
    </div>
    </div>
    </div>
</body>
</html>
